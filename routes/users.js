var express = require('express');
var router = express.Router();
var tokenUtils = require('../lib/token.utils');

/* GET users listing. */
/*router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

module.exports = router;*/


module.exports = function factory(keycloak) {
    const router = require('express').Router();
    const jwt = require('jsonwebtoken');


    // verificar authenticacion con "keycloak.protect()" o bien, implementar un middleware de keycloak como el "keycloakservice.js" de ejemplo
    // NOTA: usar los scope locales y no en "enforce" desde keycloak debido a la demanda de request sobre keycloak (o bien implementar un middleware de nodejs express)
    // ver ejemplo middles nodejs/exrpress https://stackoverflow.com/a/31718263
    /*  EJEMPLO DE TOKEN
    {
        "exp": 1639944720,
        "iat": 1639944420,
        "auth_time": 1639944035,
        "jti": "efadb8ce-8c99-47d4-ac76-898966555f52",
        "iss": "http://localhost:8080/auth/realms/app-migracion",
        "aud": [
        "app-migracion",
        "account"
        ],
        "sub": "76586e0d-a5da-4eef-a865-2d760aa30e1f",
        "typ": "Bearer",
        "azp": "app-migracion",
        "session_state": "67d3bf54-cebe-41e2-b89e-e24bacd5416f",
        "acr": "0",
        "realm_access": {
        "roles": [
        "offline_access",
        "uma_authorization"
        ]
        },
        "resource_access": {
        "app-migracion": {
        "roles": [
        "admin"
        ]
        },
        "account": {
        "roles": [
        "manage-account",
        "manage-account-links",
        "view-profile"
        ]
        }
        },
        "scope": "email migracion:ciudadano profile admin:general admin:main",
        "sid": "67d3bf54-cebe-41e2-b89e-e24bacd5416f",
        "email_verified": true,
        "name": "Manuel Mendez",
        "preferred_username": "admin",
        "given_name": "Manuel",
        "family_name": "Mendez",
        "email": "manuel.mendez.ag@gmail.com"
        }
     */
    router.get('/', keycloak.protect(), async function (req, res, next) {
        const tokenString = await tokenUtils.getTokenStringFormRequest(req);
        const tokenObject = await jwt.decode(tokenString);
        const containsAccess = await tokenUtils.isTokenContainsScope(tokenObject, ['admin:main'])
        if (!containsAccess) {
            res.status(403).json({})
        } else {
            res.send('Authentication & Authorization');
        }
    });

    return router;
}
