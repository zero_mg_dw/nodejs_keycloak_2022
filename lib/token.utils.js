module.exports = {
    getTokenStringFormRequest: async function (request) {
        if (request === null) return null;
        if (request.header('Authorization') === null) return null;
        //console.log(request.header('Authorization'));
        return request.header('Authorization').replace('Bearer ', '');
    },
    isTokenContainsScope: async function (token, scopesValues) {
        if (token === null) return false;
        if (token.scope == null) return false;
        if (token.scope.trim().length === 0) return false;
        const scopes = token.scope.split(' ');
        console.log('scopes:');
        console.log(JSON.stringify(scopes));
        const filteredArray = scopesValues.filter(value => scopes.includes(value));
        if (filteredArray === null || (filteredArray != null && filteredArray.length === 0)) return false;
        return true;
    }
}



